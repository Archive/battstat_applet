SUMMARY

Battstat is a utility that displays the status of the power managment
subsystem on laptops. It queries the APM BIOS and displays remaining battery
charge percentage in a graphical window.

Get the latest version at: http://spektr.eu.org/~jp/battstat/

FEATURES

 o   Displays remaining battery percentage in a progress bar. 
 
 o   Progress bar is color-coded to reflect the current battery charge.
 
 o   An icon display if laptop is running on AC or battery power. Or if
     it's charging the battery or if the power falls under a preset
     percentage.

 o   Sound events.

 o   Can use the status docklet.

REQUIREMENTS

GNOME: http://www.gnome.org/
An APM BIOS.


Tested with GTK+ 1.2, GNOME 1.2.1, FreeBSD 4.0-RELEASE, Debian 2.2, APM 1.2.


MISC

You may not have read access to the APM device on your system. If that is
the case there are a few different fixes. Listed below are some of them.

 o Run the program as root or set the suid bit on the application. 
   (Very bad. Who knows what horrible bugs my application might have?)

 o Modify the permissions of the device so everyone has read access to it.
   (Not very good. But if you're the only person using the laptop..)
   # chmod +r /dev/apm
    
 o Create a new apm group in /etc/group. Change the group owner of the APM
   device to that new group. Add read permissions for the group. Add 
   yourself to the group. (Better solution)
   
 o Add yourself to the same group the APM device belongs to and
   make sure that that group can read from the APM device.
   (Better solution)

If you're running FreeBSD 4.0-RELEASE and are using GTK from the
packages collection, the configure script might complain about not
finding the gtk-config utility. If that is the case, you need to make
a symbolic link from /usr/X11R6/bin/gtk12-config to
/usr/X11R6/bin/gtk-config. (Or where ever you might have GTK
installed.) Or you can edit the configure script to look for
gtk12-config instead.

If you are running Linux make sure that you have the relevant
development packages installed on your system. 

GNOME APPLET SUPPORT

If you want to install this utility as an applet follow these
steps. The paths in the paranthesis below shows the default FreeBSD
paths. If you make a "make install" that is where the files ends
up. If you want to change these catalogs and have make install it
automatically, edit the Makefile.am file. Then issue a "autoconf &&
aclocal -I macros && autoheader && automake" in the top level catalog
of the distribution. 

Manual applet install
 - Make sure you have put the executable "battstat_applet" somewhere in 
   your $PATH. ($prefix/bin)

 - Copy the file "battstat_applet.gnorba" to the correct location on
   your system. ($GNOME_PREFIX/etc/CORBA/servers)

 - Copy the file "battstat_applet.desktop" to whereever you have the
   applet configuration files on your system. 
   ($GNOME_PREFIX/share/gnome/applets/Monitors/)

 - And finally copy the icon "battstat.png" to whereever you have your
   Gnome icons.
   ($GNOME_PREFIX/share/gnome/pixmaps/)


Automatic applet install
 % ./configure ('./configure --prefix=/usr/X11R6' on FreeBSD, /usr on Debian)
 % make
 % make install
This will install the applet to the default location.

Now you should be able to select Panel->Add Applet->Monitors->Battery-
Status Utility from your system menu. You may need to restart your
Gnome session first though.

$Id: README,v 1.3 2001/03/29 21:32:00 jpehrson Exp $
