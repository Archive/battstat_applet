# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR Free Software Foundation, Inc.
# Cezary Jackiewicz <cjackiewicz@poczta.onet.pl>, 2000.
#
msgid ""
msgstr ""
"Project-Id-Version: battstat_applet\n"
"POT-Creation-Date: 2001-10-26 18:27+0200\n"
"PO-Revision-Date: nie 06 sie 2000 13:20:23 CEST\n"
"Last-Translator: Cezary Jackiewicz <cjackiewicz@poczta.onet.pl>\n"
"Language-Team: Polish <pl@li.org>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

msgid "Battery power low"
msgstr "Niski poziom energii baterii"

msgid "Battery fully re-charged"
msgstr "Bateria została w pełni naładowana"

#. Message displayed if user tries to run applet
#. on unsupported platform
#: src/battstat_applet.c:262
msgid "Your platform is not supported!\n"
msgstr ""

#: src/battstat_applet.c:263
msgid "The applet will not work properly (if at all).\n"
msgstr ""

#. The following four messages will be displayed as tooltips over
#. the battery meter.
#. High = The APM BIOS thinks that the battery charge is High.
#: src/battstat_applet.c:324 src/battstat_applet.c:827
msgid "High"
msgstr "Wysoki"

#. Low = The APM BIOS thinks that the battery charge is Low.
#: src/battstat_applet.c:326 src/battstat_applet.c:829
msgid "Low"
msgstr "Niski"

#. Critical = The APM BIOS thinks that the battery charge is Critical.
#: src/battstat_applet.c:328 src/battstat_applet.c:831
msgid "Critical"
msgstr "Krytyczny"

#. Charging = The APM BIOS thinks that the battery is recharging.
#: src/battstat_applet.c:330 src/battstat_applet.c:833
msgid "Charging"
msgstr "Trwa ładowanie"

#: src/battstat_applet.c:409
#, fuzzy, c-format
msgid "Battery low (%d%%) and AC is offline"
msgstr "Niski poziom baterii (%d%%) i zasilanie zewnętrzne jest %s"

#: src/battstat_applet.c:432
msgid "Battery is now fully re-charged!"
msgstr ""

#. This string will display as a tooltip over the status frame
#. when the computer is using battery power and the battery meter
#. and percent meter is hidden by the user.
#: src/battstat_applet.c:451 src/battstat_applet.c:1001
#, fuzzy, c-format
msgid ""
"System is running on battery power\n"
"Battery: %d%% (%s)"
msgstr ""
"System korzysta z energii %s\n"
"Stan baterii: %d%% (%s)"

#. This string will display as a tooltip over the status frame
#. when the computer is using AC power and the battery meter
#. and percent meter is hidden by the user.
#: src/battstat_applet.c:455 src/battstat_applet.c:1009
#, fuzzy, c-format
msgid ""
"System is running on AC power\n"
"Battery: %d%% (%s)"
msgstr ""
"System korzysta z energii %s\n"
"Stan baterii: %d%% (%s)"

#. This string will display as a tooltip over the status frame
#. when the computer is using battery power.
#: src/battstat_applet.c:461 src/battstat_applet.c:941
#: src/battstat_applet.c:1022
#, fuzzy
msgid "System is running on battery power"
msgstr "System korzysta z energii %s"

#. This string will display as a tooltip over the status frame
#. when the computer is using AC power.
#: src/battstat_applet.c:464 src/battstat_applet.c:947
#: src/battstat_applet.c:1028
#, fuzzy
msgid "System is running on AC power"
msgstr "System korzysta z energii %s"

#: src/battstat_applet.c:478
msgid "N/A"
msgstr ""

#. This string will display as a tooltip over the battery frame
#. when the computer is using battery power.
#: src/battstat_applet.c:609 src/battstat_applet.c:921
#, fuzzy, c-format
msgid "System is running on battery power. Battery: %d%% (%s)"
msgstr ""
"System korzysta z energii %s\n"
"Stan baterii: %d%% (%s)"

#. This string will display as a tooltip over the battery frame
#. when the computer is using AC power.
#: src/battstat_applet.c:616 src/battstat_applet.c:928
#, fuzzy, c-format
msgid "System is running on AC power. Battery: %d%% (%s)"
msgstr ""
"System korzysta z energii %s\n"
"Stan baterii: %d%% (%s)"

#. This string will display as a tooltip over the
#. battery frame when the computer is using battery
#. power and the battery isn't present. Not a
#. possible combination, I guess... :)
#: src/battstat_applet.c:627
#, fuzzy
msgid "System is running on battery power. Battery: Not present"
msgstr ""
"System korzysta z energii %s\n"
"Stan baterii: (nieznany)"

#. This string will display as a tooltip over the
#. battery frame when the computer is using AC
#. power and the battery isn't present.
#: src/battstat_applet.c:633
#, fuzzy
msgid "System is running on AC power. Battery: Not present"
msgstr ""
"System korzysta z energii %s\n"
"Stan baterii: (nieznany)"

#. Displayed as a tooltip over the battery meter when there is
#. a battery present. %d will hold the current charge and %s will
#. hold the status of the battery, (High, Low, Critical, Charging.
#: src/battstat_applet.c:642
#, c-format
msgid "Battery: %d%% (%s)"
msgstr "Stan baterii: %d%% (%s)"

#. Displayed as a tooltip over the battery meter when no
#. battery is present.
#: src/battstat_applet.c:649
msgid "Battery: Not present"
msgstr "Stan baterii: (nieznany)"

#. Displayed if the APM device couldn't be opened. (Used under *BSD)
#: src/battstat_applet.c:704
msgid ""
"Can't open the APM device!\n"
"\n"
"Make sure you have read permission to the\n"
"APM device."
msgstr ""
"Nie można otworzyć urządzenia APM!\n"
"\n"
"Upewnij się, że masz uprawnienia do odczytu stanu\n"
"urządzenia APM."

#. Displayed if the APM system is disabled (Used under *BSD)
#: src/battstat_applet.c:712
#, fuzzy
msgid ""
"The APM Management subsystem seems to be disabled.\n"
"Try executing \"apm -e 1\" (FreeBSD) and see if \n"
"that helps.\n"
msgstr ""
"Podsystem zarządzania energią (APM) wygląda na wyłączony.\n"
"Spróbuj wykonać \"apm -e 1\" (na FreeBSD) i zobacz, czy to\n"
"pomoże.\n"

#. The long name of the applet in the About dialog.
#: src/battstat_applet.c:771
msgid "Battery status utility"
msgstr "Status baterii"

#: src/battstat_applet.c:772
msgid "(C) 2000 The Gnulix Society"
msgstr ""

#. Longer description of the applet in the About dialog.
#: src/battstat_applet.c:775
msgid "This utility show the status of your laptop battery."
msgstr "Ten program pokazuje status baterii Twojego laptopa."

#. Displayed when the user tries to hide all
#. parts of the applet in the preferences
#. window.
#: src/battstat_applet.c:886
msgid "You can't hide all elements of the applet!"
msgstr ""

#. Message will be displayed if the applet couldn't be
#. created at all.
#: src/battstat_applet.c:1272
msgid "Can't create applet!\n"
msgstr "Nie można utworzyć apletu!\n"

#. Applet menu, Properties
#: src/battstat_applet.c:1450
msgid "Properties..."
msgstr "Właściwości..."

#. Applet menu, About
#: src/battstat_applet.c:1457
msgid "About..."
msgstr "Informacje o..."

#. Applet menu, Help
#: src/battstat_applet.c:1464
#, fuzzy
msgid "Help"
msgstr "Pomoc..."

#. Applet menu, Suspend laptop
#: src/battstat_applet.c:1470
msgid "Suspend laptop"
msgstr ""

#. Message in a Gnome warning dialog window
#. when the user enables the suspend
#. function.
#: src/properties.c:123
msgid ""
"You have chosen to enable the Suspend function. This can potentially be a "
"security risk."
msgstr ""

#: battstat_applet.desktop.in.h:1
msgid "Battery Status Utility"
msgstr "Status baterii"

#: battstat_applet.desktop.in.h:2
#, fuzzy
msgid "Shows the status of the battery"
msgstr "Ten program pokazuje status baterii Twojego laptopa."

#, fuzzy
#~ msgid "Battery low (%d%%) and AC is online"
#~ msgstr "Niski poziom baterii (%d%%) i zasilanie zewnętrzne jest %s"

#~ msgid "Battstat Settings"
#~ msgstr "Ustawienia apletu Battstat"

#~ msgid "General"
#~ msgstr "Ogólne"

#~ msgid "Battery color levels (%)"
#~ msgstr "Kolory poziomów baterii (%)"

#~ msgid "Yellow:"
#~ msgstr "Żółty:"

#~ msgid "Orange:"
#~ msgstr "Pomarańczowy:"

#~ msgid "Red:"
#~ msgstr "Czerwony:"

#~ msgid "Preview"
#~ msgstr "Podgląd"

#~ msgid "Warnings"
#~ msgstr "Ostrzeżenia"

#~ msgid "Percentage text:"
#~ msgstr "Tekst procentu:"

#~ msgid "Appearance"
#~ msgstr "Wygląd"

#~ msgid "Progress bar direction"
#~ msgstr "Kierunek wskaźnika naładowania"

#~ msgid "Move towards top"
#~ msgstr "Do góry"

#~ msgid "Move towards bottom"
#~ msgstr "Do dołu"

#~ msgid "AC"
#~ msgstr "sieciowej."

#~ msgid "battery"
#~ msgstr "baterii."

#~ msgid "online"
#~ msgstr "włączone"

#~ msgid "offline"
#~ msgstr "wyłączone"

#~ msgid "System is running on %s power."
#~ msgstr "System korzysta z energii %s"

#~ msgid "Orientation"
#~ msgstr "Orientacja"

#~ msgid "Horizontal"
#~ msgstr "Poziomo"

#~ msgid "Square"
#~ msgstr "Kwadrat"

#~ msgid "Low battery notification"
#~ msgstr "Niski poziom energii baterii"

#~ msgid "Full battery notification"
#~ msgstr "Pełne naładowanie baterii"

#~ msgid "Beep"
#~ msgstr "Sygnał dźwiękowy"

#~ msgid "Layout"
#~ msgstr "Rozmieszczenie"

#~ msgid "On"
#~ msgstr "Włączone"

#~ msgid "Off"
#~ msgstr "Wyłączone"
